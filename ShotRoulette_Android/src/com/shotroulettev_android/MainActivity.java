package com.shotroulettev_android;

import java.util.ArrayList;
import java.util.Random;



import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	

	EditText mTextField;
	Button startButton;
	Button gameprefs;
	Button addPlauyers;
	Button viewPlayers;
	boolean flag = true;
	MediaPlayer soundNotification;
	Random randomnumber  = new Random();
	int randomChoice;
	String timeinmins;
	int timeinmilliseconds;
	String timeinSeconds;
	int timeinseconds;
	
	//MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.gong);
    
	final Context context = this;
  
	ArrayList<String> list = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	
		
		        //Create Button and Button Listeners for startButton
				Button startButton = (Button)findViewById(R.id.startButton);
				startButton.setOnClickListener(startButton_listener);
				
				//Add Players Button
				Button addPlayersButton = (Button)findViewById(R.id.addPlayersButton);
				addPlayersButton.setOnClickListener(addPlayersButton_listener);
				
				//Add ViewPlayers Button
				Button viewPlayersButton = (Button)findViewById(R.id.viewPlayersButton);
				viewPlayersButton.setOnClickListener(ViewPlayersButton_listener);
				
				//Game Prefs BUtton
				Button gameprefs = (Button)findViewById(R.id.preferencesButton);
				gameprefs.setOnClickListener(gameprefsButton);
				
				mTextField = (EditText)findViewById(R.id.editText1);
		
	}
	
	//Kill APP When user presses back button
	public boolean onKeyDown(int keycode, KeyEvent event) {
	    if (keycode == KeyEvent.KEYCODE_HOME || keycode == KeyEvent.KEYCODE_BACK) {
	       finish();
	    }
	    return super.onKeyDown(keycode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

 /******* ONCLICK LISTENER IMPLEMENTATIONS *********/
	
	OnClickListener startButton_listener = new OnClickListener(){

		@Override
		public void onClick(View arg0) {
			
			final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.gong);
			
			Context context = getApplicationContext();
			int duration = Toast.LENGTH_SHORT;
			
			if(list.isEmpty()){
				 Toast noPlayers = Toast.makeText(context, "Please add players to the round!",duration);
				 noPlayers.show();
				 return ;
				}
			   
			
			Toast toast = Toast.makeText(context,"The Round Begins!", duration);
			toast.show();
			
	
			
			new CountDownTimer(timeinseconds * 1000, 1000) {

			     public void onTick(long millisUntilFinished) {
			         mTextField.setText( "Time Remaining: " + millisUntilFinished / 1000);
			         if ( (millisUntilFinished/1000)%5 == 0 ){
			        	 randomChoice = randomnumber.nextInt(list.size());
			        	 
			        	
			        	 
			        	 Toast toast = Toast.makeText(getApplicationContext(), list.get(randomChoice) + " MUST TAKE A SHOT!",Toast.LENGTH_SHORT);
			        	 toast.show();
			        	 
			        	 
			        	
			     	   mp.start();
			        	 
			 
			         }
			         
			     }

			     public void onFinish() {
			    	 mp.start();
			         mTextField.setText("ROUND FINISHED!");
			     }
			  }.start();
			 
			
		}
		
		
		
	};
	
// Add Players Button Listener!
	
	OnClickListener addPlayersButton_listener = new OnClickListener(){

		@Override
		public void onClick(View arg0) {
		    
			
			//Create an input dialog
			AlertDialog.Builder addPlayers_alert = new AlertDialog.Builder(context);
			addPlayers_alert.setTitle("Add Player");
			addPlayers_alert.setMessage("Please Enter Player Name:" );
			
			//Set an EditText view to get user input
			final EditText input = new EditText(context);
			addPlayers_alert.setView(input);
			
			//Set Postive Button of Alert
			addPlayers_alert.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
				@SuppressLint("NewApi")
				public void onClick(DialogInterface dialog, int whichButton){
					String playerName = input.getText().toString();
					
					if(playerName.isEmpty() == false){
					list.add(playerName);// Add player to list
					Toast alert = Toast.makeText(context,playerName + " has been added!", Toast.LENGTH_SHORT);
					alert.setGravity(Gravity.BOTTOM, 10, 4);
					alert.show();	
					return;
					}
					
					else{
						
						
						Toast playerNotification = Toast.makeText(context,"Please add a player name!", Toast.LENGTH_SHORT);
						playerNotification.setGravity(Gravity.BOTTOM, 10, 4);
						playerNotification.show();	
					}
					
					
				}
			});
			
			//Set Negative Button of Alert
			addPlayers_alert.setNegativeButton("Done", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int whichButton){
				// cancel
					
					dialog.cancel();
				    
				}
			});
			
	    
		addPlayers_alert.show();
		
	    }
			
			 
		
		 
		
	};// End of AddPlayersButton Listener
	

OnClickListener ViewPlayersButton_listener = new OnClickListener(){

	@Override
	public void onClick(View arg0) {
		
		//Add New Intent
		
		 Intent new_intent =  new Intent (getApplicationContext(),ViewPlayers.class);
		    
		    new_intent.putExtra("list_of_players", list);
		    
		    startActivity(new_intent);
		
	}};

OnClickListener  gameprefsButton = new OnClickListener(){

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		//Create an input dialog
		AlertDialog.Builder addPlayers_alert = new AlertDialog.Builder(context);
		addPlayers_alert.setTitle("Time Interval");
		addPlayers_alert.setMessage(" Please Set Round Interval (In Seconds)" );
		
		//Set an EditText view to get user input
		final EditText input = new EditText(context);
		addPlayers_alert.setView(input);
		
		//Set Postive Button of Alert
		addPlayers_alert.setPositiveButton("Ok", new DialogInterface.OnClickListener(){
			@SuppressLint("NewApi")
			public void onClick(DialogInterface dialog, int whichButton){
				String timeinSeconds = input.getText().toString();
				
			  if(timeinSeconds.isEmpty() == false){
				timeinseconds = Integer.parseInt(timeinSeconds);
			  }
				
				else{
					
					
					Toast playerNotification = Toast.makeText(context,"Pl", Toast.LENGTH_SHORT);
					playerNotification.setGravity(Gravity.BOTTOM, 10, 4);
					playerNotification.show();	
				}
				
				
			}
		});
		
		//Set Negative Button of Alert
		addPlayers_alert.setNegativeButton("Done", new DialogInterface.OnClickListener(){
		public void onClick(DialogInterface dialog, int whichButton){
			// cancel
				
				dialog.cancel();
			    
			}
		});
		
    
	addPlayers_alert.show();
		
	}
	
	
	


};
	
	

}

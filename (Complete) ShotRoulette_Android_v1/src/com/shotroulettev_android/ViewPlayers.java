package com.shotroulettev_android;

import java.util.ArrayList;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ViewPlayers extends ListActivity{
	
	ListView listview;
	
	
    ArrayList<String> player_list = new ArrayList<String>();
    
    /** Declaring an ArrayAdapter to set items to ListView*/
    ArrayAdapter<String> adapter;
    
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.viewplayers_layout);
	    // TODO Auto-generated method stub
	    
	    adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, player_list);
	    
	    ArrayList<String> playerList = getIntent().getExtras().getStringArrayList("list_of_players");
	    
	    Log.w("WOWO", "HELLO");
	    
	    
	    	for(int i = 0; i< playerList.size(); i++){
	    		player_list.add(playerList.get(i));
	    		adapter.notifyDataSetChanged();		
	    	}
	   
	  
	    //Setting the adapter to the ListView
	    setListAdapter(adapter);
	    
	   listview = getListView();
	   
	    
	    //Create onLongClickListerner to delete player after long press
	   listview.setOnItemLongClickListener(new OnItemLongClickListener() {

           public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                   int pos, long id) {
        	   
        	   
        	   
               // TODO Auto-generated method stub
               Log.v("long clicked","pos"+" "+pos);
               player_list.remove(pos);
               adapter.notifyDataSetChanged();
               
        

       
               return true;
           }
       }); 
	}

}
